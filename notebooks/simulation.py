import numpy as np


def trajectory_gen(P, R, s0=None):
    """Create a generator that yields steps in a trajectory."""
    if s0 is None:
        s = 1
    else:
        s = s0
    # Problem setup
    ns = len(P)
    states = np.arange(ns)
    I = np.eye(ns)

    # Initial state
    x = np.zeros(ns)
    x[s] = 1
    while True:
        # Sample the next state
        dp = np.dot(x, P)
        sp = np.random.choice(states, p=dp)
        xp = I[sp]
        r = R[s, sp]

        # Yield the result
        yield (s, r, sp)
        # Set up for the next iteration
        s = sp
        x = xp


def compute_return(steps, gmfunc):
    """Compute the Monte-Carlo return given a list of steps of the form
    `(s, a, sp)` and a function `gmfunc`, returns a list with form `(s, g, sp)`.
    """
    ret = []
    g = 0
    for step in reversed(steps):
        s, r, sp = step
        g = r + gmfunc(sp)*g
        ret.append((s, g, sp))
    return list(reversed(ret))

def compute_lambda_return(steps, gmfunc, lmfunc, vfunc):
    pass
