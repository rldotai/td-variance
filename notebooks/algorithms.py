import numpy as np
from numpy.linalg import pinv


class TD:
    pass


class TamarTD:
    pass


class TDVarFixed:
    pass


class TDVar:
    pass


class TDVarTraces:
    """Learning value function and variance (WITH CORRECTING TRACES)

    Excessively parameterized, for trying out many variants of the algorithm.
    """
    def __init__(self, n):
        """Initialize the learning algorithm.

        Parameters
        -----------
        n : int
            The number of features, i.e. expected length of the feature vector.
        """
        self.n = n
        self.reset()

    def get_value(self, x):
        """Get the approximate value for feature vector `x`."""
        return np.dot(self.theta, x)

    def get_variance(self, x):
        """Get the approximate variance for feature vector `x`."""
        return np.dot(self.w, x)

    def update(self, x, r, xp, alpha, gm, gm_p, lm,
               v_gm, v_gm_p, v_lm, v_lm_p, v_alpha, v_beta, v_eta):
        """Update from new experience, i.e. from a transition `(x,r,xp)`."""
        # Value estimation updates
        delta = r + gm_p * np.dot(self.theta, xp) - np.dot(self.theta, x)
        self.e = x + gm*lm*self.e
        self.theta += alpha*delta*self.e

        # Variance-estimation updates
        gmbar = v_gm_p*v_lm_p
        dtbar = delta**2 + (gmbar**2)*np.dot(self.w, xp) - np.dot(self.w, x)
        self.z = x + v_eta*v_gm*self.z
        self.w += v_alpha*dtbar*self.z - v_beta*delta*self.h
        self.h = v_gm_p*v_lm_p*(x*delta + self.h)

    def reset(self):
        # Value estimation related
        self.theta = np.zeros(self.n)
        self.e = np.zeros(self.n)

        # Variance-estimation quantities
        self.w = np.zeros(self.n)
        self.z = np.zeros(self.n)
        self.h = np.zeros(self.n)

class TDVarSqrt:
    """Learning value function and variance with square root correction.

    Excessively parameterized, for trying out many variants of the algorithm.
    """
    def __init__(self, n):
        """Initialize the learning algorithm.

        Parameters
        -----------
        n : int
            The number of features, i.e. expected length of the feature vector.
        """
        self.n = n
        self.reset()

    def get_value(self, x):
        """Get the approximate value for feature vector `x`."""
        return np.dot(self.theta, x)

    def get_variance(self, x):
        """Get the approximate variance for feature vector `x`."""
        return np.dot(self.w, x)

    def update(self, x, r, xp, alpha, gm, gm_p, lm,
               v_gm, v_gm_p, v_lm, v_lm_p, v_alpha, v_beta, v_eta):
        """Update from new experience, i.e. from a transition `(x,r,xp)`."""
        # Value estimation updates
        delta = r + gm_p * np.dot(self.theta, xp) - np.dot(self.theta, x)
        self.e = x + gm*lm*self.e
        self.theta += alpha*delta*self.e

        # Variance-estimation updates
        gmbar = (v_gm_p*v_lm_p)
        dtbar = delta**2 + (gmbar**2)*np.dot(self.w, xp) - np.dot(self.w, x)
        varsp = np.dot(self.w, xp)
        droot = 2*gmbar*delta*np.sqrt(np.max(varsp, 0))
        self.z = x + v_eta*v_gm*self.z
        self.w += v_alpha*dtbar*self.z + v_beta*droot*self.z

    def reset(self):
        # Value estimation related
        self.theta = np.zeros(self.n)
        self.e = np.zeros(self.n)

        # Variance-estimation quantities
        self.w = np.zeros(self.n)
        self.z = np.zeros(self.n)
